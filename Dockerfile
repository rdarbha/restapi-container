# syntax=docker/dockerfile:1

FROM python:3-slim
WORKDIR /app
ADD requirements.txt /app
ADD ./app/front_end.py /app
RUN python -m pip install -r requirements.txt
#CMD ["gunicorn", "-w 4", "-b", "0.0.0.0:8000", "--chdir", "/app", "front_end:app"]
CMD ["hypercorn", "-b", "0.0.0.0:8000", "front_end:app"]
EXPOSE 8000